package example;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;

public class ButtonSomething extends JFrame{

	private JLabel response; 
	
	public ButtonSomething() {
		init();
	}
	
	private void init() {
	
		// frame initialization
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("something");
		this.setSize(400, 400);
		
		// components initialization
		JButton b1 = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		
		b1.addActionListener(new ClickListener());
		b2.addActionListener(new ClickListener());
		b3.addActionListener(new ClickListener());
		b4.addActionListener(new ClickListener());
		
		response = new JLabel("something");
		
		JComponent[] comps = {b1, b2, b3, b4, response};
		
		createLayout(comps);
		
		this.setVisible(true);
		this.pack();
	}
	
	// creates components layout and adds components
	private void createLayout(JComponent comps[]) {
		
		Container pane = this.getContentPane();
		pane.setLayout(new MigLayout());
		
		for (JComponent c: comps) {
			pane.add(c, "span");
		}
	}
	
	// handles click action
	private class ClickListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton source = (JButton) e.getSource();
			response.setText(source.getText());
		}
		
	}
	
	public static void main(String [] args) {
		ButtonSomething smt = new ButtonSomething();
	}
}
