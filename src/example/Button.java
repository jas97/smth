package example;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Button extends JFrame{
	
	public Button() {
		init();
	}
	
	public void init() {
		
		createLayout();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(200,200);
		this.setVisible(true);
		this.setTitle("Button");
	}
	
	private void createLayout() {
		
		Container pane = this.getContentPane();
		JButton button = new MyButton("button");
		pane.add(button);
		
	}
	
	private class MyButton extends JButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			System.exit(0);
			
		}
		
		public MyButton(String text) {
			this.setText(text);
			this.setSize(20,20);
			this.addActionListener(this);
		}
	}
	
	public static void main (String [] args) {
		Button b = new Button();
	}
}
