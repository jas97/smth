package example;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Panel {
	
	public static void main(String [] args) {
		
		JFrame frame = new JFrame("example");
		frame.setSize(350, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		
		frame.add(panel);
		
		placeComponents(panel);
		
		frame.setVisible(true);
	}

	public static void placeComponents(JPanel panel) {
		
		panel.setLayout(null);
		
		JLabel userLabel = new JLabel("user");
		userLabel.setBounds(10, 20, 80, 25);
		panel.add(userLabel);
		
		JLabel passLabel = new JLabel("password");
		passLabel.setBounds(10, 50, 80,25);
		panel.add(passLabel);
		
		JTextField userText = new JTextField(20);
		userText.setBounds(100, 20, 165, 25);
		panel.add(userText);
		
		JTextField passText = new JTextField(20);
		passText.setBounds(100, 50, 165, 25);
		panel.add(passText);
		
		
		
	}
	
	private class MyButton extends JButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			System.exit(0);
			
		}
		
		public MyButton(String text) {
			this.setText(text);
			this.addActionListener(this);
		}
		
	}
}
