package mvcSwift;

/*
 * Model of a person.
 */

class PersonModel {

    private Person person;

    public PersonModel(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }

    @Override
    public String toString() {
        return person.getFirstName() + ", " + person.getLastName();
    }
}
