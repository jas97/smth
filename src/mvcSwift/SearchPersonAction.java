package mvcSwift;

import java.awt.event.ActionEvent;
import java.util.LinkedList;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/*
 * Controller.
 * Reacts to a button being pressed.
 * Finds a person whose info is in the document searchInput.
 */

class SearchPersonAction extends AbstractAction {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Document searchInput;
    private DefaultListModel<PersonModel> searchResult;
    private PersonService personService;

    public SearchPersonAction(Document searchInput, DefaultListModel<PersonModel> searchResult, PersonService personService) {
        this.searchInput = searchInput;
        this.searchResult = searchResult;
        this.personService = personService;
    }

    public void actionPerformed(ActionEvent e) {
        String searchString = getSearchString();

        LinkedList<Person> matchedPersons = personService.searchPersons(searchString);

        searchResult.clear();
        for (Person person : matchedPersons) {
            PersonModel elementModel = new PersonModel(person);
            searchResult.addElement(elementModel);
        }
    }

    private String getSearchString() {
        try {
            return searchInput.getText(0, searchInput.getLength());
        } catch (BadLocationException e) {
            return null;
        }
    }

}
