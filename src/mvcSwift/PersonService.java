package mvcSwift;

import java.util.LinkedList;

/*
 * Declares what should be able to be done with models.
 */

interface PersonService {

    LinkedList<Person> searchPersons(String searchString);
}

