package mvcSwift;

import java.awt.BorderLayout;
import java.awt.Container;


import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/*
 * View
 */

public class Main {

    public static void main(String[] args) {
    	
    	// sets up the frame
        JFrame mainFrame = new JFrame("MVC example");
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setSize(640, 300);
        mainFrame.setLocationRelativeTo(null);

        PersonService personService = new PersonServiceMock();

        //instances of models
        DefaultListModel<PersonModel> searchResultListModel = new DefaultListModel<PersonModel>();
        DefaultListSelectionModel searchResultSelectionModel = new DefaultListSelectionModel();
        searchResultSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        Document searchInput = new PlainDocument();

        // instances of controllers
        PersonDetailsAction personDetailsAction = new PersonDetailsAction(searchResultSelectionModel, searchResultListModel);
        SearchPersonAction searchPersonAction = new SearchPersonAction(searchInput, searchResultListModel, personService);
        
        Container contentPane = mainFrame.getContentPane();

        // adds components to panels, and attaches listeners to components
        JPanel searchInputPanel = new JPanel();
        searchInputPanel.setLayout(new BorderLayout());

        JTextField searchField = new JTextField(searchInput, null, 0);
        searchInputPanel.add(searchField, BorderLayout.CENTER);
        searchField.addActionListener(searchPersonAction);

        JButton searchButton = new JButton("search");
        searchButton.addActionListener(searchPersonAction);
        searchInputPanel.add(searchButton, BorderLayout.EAST);

        JList<PersonModel> searchResultList = new JList<PersonModel>();
        searchResultList.setModel(searchResultListModel);
        searchResultList.setSelectionModel(searchResultSelectionModel);

        
        JPanel searchResultPanel = new JPanel();
        
        searchResultPanel.setLayout(new BorderLayout());
        JScrollPane scrollableSearchResult = new JScrollPane(searchResultList);
        searchResultPanel.add(scrollableSearchResult, BorderLayout.CENTER);

        JPanel selectionOptionsPanel = new JPanel();

        JButton showPersonDetailsButton = new JButton("show details");
        showPersonDetailsButton.addActionListener(personDetailsAction);
        selectionOptionsPanel.add(showPersonDetailsButton);

        //adds panels to content pane
        contentPane.add(searchInputPanel, BorderLayout.NORTH);
        contentPane.add(searchResultPanel, BorderLayout.CENTER);
        contentPane.add(selectionOptionsPanel, BorderLayout.SOUTH);

        mainFrame.setVisible(true);
    }
}